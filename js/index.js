"use strict"

/*

1-
setTimeout викликається один раз, через певний проміжок часу. а setInterval викликається постійно, через певний проміжок часу (типу цикл).

2-
clearTimeout() викликається для setTimeout. в аргумент clearTimeout() записується посилання на цей самий сет.
clearInterval() викликається для setInterval в аргумент clearInterval() записується посилання на цей самий сет.

---------------------------------------------------------------------------------------------------------------------------------
*/

//1-

const buttonOperation = document.querySelector(".click-B");
const textOperation = document.querySelector(".click-D");

buttonOperation.addEventListener("click",() => {
    setTimeout(() =>{
        textOperation.innerText = "операція виконана успішно"
    }, 3000);
});

//2-
const timer = document.querySelector(".timer");
let x = 11;

const timerInterval = setInterval(() => {
    x--
    timer.innerText = x;
    if(x === 0) {
        clearInterval(timerInterval);
        timer.innerText = "Зворотній відлік завершено";
    }
}, 1000);